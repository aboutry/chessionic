import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { BoardRepresentation } from 'src/app/representations/board-representation.model';
import { BoardChunkRepresentation } from 'src/app/representations/board-chunk-representation.model';
import { PieceRepresentation } from 'src/app/representations/piece-representation.model';

@Injectable({
  providedIn: 'root'
})
export class PartieService {
  private currentPlayer: Subject<string> = new BehaviorSubject(undefined);
  private chessBoard: Subject<BoardRepresentation> = new BehaviorSubject(undefined);
  private chessBoardChrunked: Subject<BoardChunkRepresentation> = new BehaviorSubject(undefined);

  public observableCurrentPlayer$ = this.currentPlayer.asObservable();
  public observablechessBoard$ = this.chessBoard.asObservable();
  public observablechessBoardChrunked$ = this.chessBoardChrunked.asObservable();

  constructor(private http: HttpClient) {
    this.currentPlayer.next("white");
  }

  public getDefaultBoardJson(): Observable<any> {
    return this.http.get("../../../assets/json/defaultBoard.json");
  }

  public setCurrentPlayer(player: string) {
    this.currentPlayer.next(player);
  }

  public getCurrentPlayer(): Observable<string> {
    return this.currentPlayer;
  }

  public setChessBoard(newBoard: BoardRepresentation) {
    this.chessBoard.next(newBoard);
    this.setChessBoardChrunked(new BoardChunkRepresentation(newBoard));
  }

  public getChessBoard(): Observable<BoardRepresentation> {
    return this.chessBoard;
  }

  public setChessBoardChrunked(newBoard: BoardChunkRepresentation) {
    this.chessBoardChrunked.next(newBoard);
  }

  public getChessBoardChrunked(): Observable<BoardChunkRepresentation> {
    return this.chessBoardChrunked;
  }

  // Place les pieces sur le plateau et le retourne
  public setPiecesOnBoard(board: BoardRepresentation, pieces: Array<PieceRepresentation>): Array<PieceRepresentation>
  {
    let newBoard = new Array<PieceRepresentation>();
    board.cases.forEach(boardCase => {
      let newBoardCase;
      pieces.forEach(piece => {
        if(piece.position === boardCase.position)
        {
          newBoardCase = piece;
        }
      });
      if(!newBoardCase)
      {
        newBoardCase = new PieceRepresentation(null, boardCase.position, null);
      }
      newBoard.push(newBoardCase);
    });
    return newBoard;
  } 
}