import { PieceRepresentation } from './../../representations/piece-representation.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MoveService {

  abscisses = ["a", "b", "c", "d", "e", "f", "g", "h"];
  abscisse: number;
  ordonnee: number;
  color: string;
  positions: Array<string>;
  piecesList: Array<PieceRepresentation>;

  constructor() { }

  // select letter from position and turn it into number
  private setAbscisse(piece: PieceRepresentation){
    this.abscisse = this.abscisses.indexOf(piece.position.substr(0,1));
  }

  // select number from position
  private setOrdonnee(piece: PieceRepresentation){
    this.ordonnee = Number(piece.position.substr(1,1));
  }

  /* To know if position is available, and if the piece can move further : 
                            0 --> position empty, 
                            1 --> position with a same color piece, 
                            2 --> position with the other color piece */
  private occupation(move: string, pawn: number = 0): number{
    let code: number = 0;
    this.piecesList.forEach(piece => {
      if (piece.isAlife && move === piece.position){
        code = (piece.color === this.color) ? 1 : 2;
      }
    });
    // piece can move except pawn diagonally or move and eat except pawn vertically
    if ((code === 0 && pawn !== 2) || (code === 2 && pawn !== 1)){
      this.positions.push(move);
    }
    return code;
  }

  private orthogonalMove(){
    // add horizontal move towards left
    let x = this.abscisse;
    let y = this.ordonnee;
    let coordinates;
    while (x > 0){
      x --;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add horizontal move towards right
    x = this.abscisse;
    while (x < 7){
      x ++;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add vertical move towards down
    x = this.abscisse;
    while (y > 1){
      y --;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add vertical move towards up
    y = this.ordonnee;
    while (y < 8){
      y ++;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
  }

  private diagonalMove(){
    // add move from right to left and up to down
    let x = this.abscisse;
    let y = this.ordonnee;
    let coordinates;
    while (x > 0 && y > 1){
      x --;
      y --;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add move from left to right and down to up
    x = this.abscisse;
    y = this.ordonnee;
    while (x < 7 && y < 8){
      x ++;
      y ++;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add move from right to left and down to up
    x = this.abscisse;
    y = this.ordonnee;
    while (x > 0 && y < 8){
      x --;
      y ++;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
    // add move from left to right and up to down
    x = this.abscisse;
    y = this.ordonnee;
    while (x < 7 && y > 1){
      x ++;
      y --;
      coordinates = this.abscisses[x]+y;
      if (this.occupation(coordinates) != 0){
        break;
      }
    }
  }

  private getMovesPiece(piece: PieceRepresentation, piecesList: Array<PieceRepresentation>): Array<string> {
    this.positions = [];
    this.setAbscisse(piece);
    this.setOrdonnee(piece);
    this.color = piece.color;
    this.piecesList = piecesList;
    switch (piece.type) {
      case "king": // test each of 8 positions around it
        if (this.abscisse !== 0){
          this.occupation(this.abscisses[this.abscisse-1]+this.ordonnee);
          if(this.ordonnee !== 1){
            this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee-1));
          }
          if(this.ordonnee !== 8){
            this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee+1));
          }
        }
        if (this.abscisse !== 7){
          this.occupation(this.abscisses[this.abscisse+1]+this.ordonnee);
          if(this.ordonnee !== 1){
            this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee-1));
          }
          if(this.ordonnee !== 8){
            this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee+1));
          }
        }
        if(this.ordonnee !== 1){
          this.occupation(this.abscisses[this.abscisse]+(this.ordonnee-1));
        }
        if(this.ordonnee !== 8){
          this.occupation(this.abscisses[this.abscisse]+(this.ordonnee+1));
        }
        if(piece.initiale){
          this.addRoque(piece, piecesList);
        }
        break;

      case "queen":
        this.orthogonalMove();
        this.diagonalMove();
        break;

      case "bishop":
        this.diagonalMove();
        break;

      case "knight": // test each of 8 positions in relation to its particular move
        if (this.abscisse !== 0){
          if(this.ordonnee > 2){
            this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee-2));
          }
          if(this.ordonnee < 7){
            this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee+2));
          }
        }
        if (this.abscisse !== 7){
          if(this.ordonnee > 2){
            this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee-2));
          }
          if(this.ordonnee < 7){
            this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee+2));
          }
        }
        if (this.abscisse > 1){
          if(this.ordonnee !== 1){
            this.occupation(this.abscisses[this.abscisse-2]+(this.ordonnee-1));
          }
          if(this.ordonnee !== 8){
            this.occupation(this.abscisses[this.abscisse-2]+(this.ordonnee+1));
          }
        }
        if (this.abscisse < 6){
          if(this.ordonnee !== 1){
            this.occupation(this.abscisses[this.abscisse+2]+(this.ordonnee-1));
          }
          if(this.ordonnee !== 8){
            this.occupation(this.abscisses[this.abscisse+2]+(this.ordonnee+1));
          }
        }
        break;

      case "tower":
        this.orthogonalMove();
        break;

      case "pawn":
        // white pawns just can go up
        if (this.color === "white"){
          if(this.ordonnee !== 8){ // can't eat from these moves => 1 in occupation()
            if(this.occupation(this.abscisses[this.abscisse]+(this.ordonnee+1), 1) === 0 && this.ordonnee === 2){
              this.occupation(this.abscisses[this.abscisse]+(this.ordonnee+2), 1);
            }
            // can only eat from these moves => 2 in occupation()
            if(this.abscisse != 0){
              this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee+1), 2);
            }
            if(this.abscisse != 7){
              this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee+1), 2);
            }
          }
        } else {
           // black pawns just can go down
          if(this.ordonnee !== 1){ // can't eat from these moves => 1 in occupation()
            if(this.occupation(this.abscisses[this.abscisse]+(this.ordonnee-1), 1) === 0 && this.ordonnee === 7){
              this.occupation(this.abscisses[this.abscisse]+(this.ordonnee-2), 1);
            }
            // can only eat from these moves => 2 in occupation()
            if(this.abscisse != 0){
              this.occupation(this.abscisses[this.abscisse-1]+(this.ordonnee-1), 2);
            }
            if(this.abscisse != 7){
              this.occupation(this.abscisses[this.abscisse+1]+(this.ordonnee-1), 2);
            }
          }
        }
        break;
    
      default:
        break;
    }    
    return this.positions;
  }
  
  // Ajoute les roques
  addRoque(piece: PieceRepresentation, piecesList: Array<PieceRepresentation>) {
    let deplacementsOpponents = [];
    let tempMoveService = new MoveService();

    piecesList.forEach(element => {
      if(element.color != piece.color && element.type != 'king')
      {
        tempMoveService.getMovesPiece(element, piecesList).forEach(moves => {
          if(deplacementsOpponents.indexOf(moves) === -1){
            deplacementsOpponents.push(moves);
          }
        });
      }
    });

    if(piece.color === 'white')
    {
      if(piecesList[63].initiale && !piecesList[62].type && !piecesList[61].type
        && deplacementsOpponents.indexOf('f1') === -1 && deplacementsOpponents.indexOf('g1') === -1)
      {
        this.positions.push('g1');
      }
      if(piecesList[56].initiale && !piecesList[59].type && !piecesList[58].type && !piecesList[57].type
        && deplacementsOpponents.indexOf('d1') === -1 && deplacementsOpponents.indexOf('c1') === -1)
      {
        this.positions.push('c1');
      }
    }
    else
    {
      if(piecesList[0].initiale && !piecesList[1].type && !piecesList[2].type && !piecesList[3].type
        && deplacementsOpponents.indexOf('d8') === -1 && deplacementsOpponents.indexOf('c8') === -1)
      {
        this.positions.push('c8');
      }
      if(piecesList[7].initiale && !piecesList[6].type && !piecesList[5].type
        && deplacementsOpponents.indexOf('f8') === -1 && deplacementsOpponents.indexOf('g8') === -1)
      {
        this.positions.push('g8');
      }
    }
  }

  getAvailableMoves(piece: PieceRepresentation, piecesList: Array<PieceRepresentation>): Array<string> {
    // check available positions, remove these causing own mate, then return the list
    let tempMoveService = new MoveService();
    return tempMoveService.notCauseOwnMate(piece, piecesList, this.getMovesPiece(piece, piecesList));
  }

  private notCauseOwnMate(piece: PieceRepresentation, piecesList: Array<PieceRepresentation>, positions: Array<string>): Array<string> {
    let positionsToRemove: Array<string> = [];
    this.piecesList = [...piecesList];
    // replace real piece with a copy to test future positions
    let pieceToMove = new PieceRepresentation(piece.type, piece.position, piece.color);
    this.piecesList.splice(this.piecesList.indexOf(piece), 1);
    this.piecesList.push(pieceToMove);
    // we scan each position available
    positions.forEach(position => {
      pieceToMove.position = position;
      //for each position, we check if an opponent piece cause mate
      this.piecesList.forEach(element => {
        if (piece.color !== element.color) {
          if (this.causeOpponentMate(element, this.piecesList)){
            positionsToRemove.push(position);
          }
        }
      });
    });
    
    if (positionsToRemove.length !== 0) {
      positionsToRemove.forEach(position => {
        positions.splice(positions.indexOf(position), 1);
      });
    }
    return positions;
  }

  causeOpponentMate(piece: PieceRepresentation, piecesList: Array<PieceRepresentation>): boolean {
    let tempMoveService = new MoveService();
    let positions: Array<string> = tempMoveService.getMovesPiece(piece, piecesList);
    let isMate: boolean = false;
    piecesList.forEach(element => {
      if (element.isAlife && element.color !== piece.color && element.type === "king" && positions.indexOf(element.position) !== -1){
        isMate = true;
      }
    });
    return isMate;
  }

  public getPromo(piece: PieceRepresentation): boolean
  {
    if(piece.type === 'pawn' && 
    (piece.color === 'black' && piece.position[1] === "1"
    || piece.color === 'white' && piece.position[1] === '8'))
    {
      return true;
    }
    return false;
  }

  public getMate(piecesList: Array<PieceRepresentation>): boolean {
    let isMate: boolean = false;
    piecesList.forEach(piece => {
      if (this.causeOpponentMate(piece, piecesList)){
        isMate = true;
      }
    });
    return isMate;
  }

  public getGameOver(piecesList: Array<PieceRepresentation>, colorNewPlayer: string): boolean {
    let isNotPosition: boolean = true;
    let tempMoveService = new MoveService();
    piecesList.forEach(piece => {
      // we check if new player can move at least one piece (if yes he play, else it's game over)
      if (piece.color === colorNewPlayer){
        if (tempMoveService.getAvailableMoves(piece, piecesList).length !== 0){
          isNotPosition = false;
        }
      }
    });
    return isNotPosition;
  }
}

