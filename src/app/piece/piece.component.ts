import { Component, Input, OnInit } from '@angular/core';
import { PieceRepresentation } from './../representations/piece-representation.model';

@Component({
  selector: 'app-piece',
  templateUrl: './piece.component.html',
  styleUrls: ['./piece.component.scss'],
})
export class PieceComponent implements OnInit {
  private _type: string;
  private _color: string;

  @Input() set color(pieceColor: string) {
    this._color = pieceColor;
    this.render();
  }
  @Input() set type(pieceType: string) {
    this._type = pieceType;
    this.render();
  }

  image: string;
  description: string;

  constructor() { 
  }

  ngOnInit() {
  }

  // render de l'affichage
  render(){
    this.getImage();
    this.getDescription();
  }

  getImage(){  
    this.image = (this._type && this._color) ? './../../assets/img/' + this._color + this._type + '.svg' : null;
  }

  getDescription(){
    this.description = (this._type && this._color) ? this._color + ' ' + this._type : null;
  }
}
