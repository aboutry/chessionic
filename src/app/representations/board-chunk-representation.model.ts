import { PieceRepresentation } from './piece-representation.model';
import { BoardRepresentation } from './board-representation.model';

/**
 * BoardChunkRepresentation
 *
 * @export
 * @class BoardChunkRepresentation
 */
export class BoardChunkRepresentation {
    public cases: Array<Array<PieceRepresentation>>;

    // Transforme le plateau en un tableau 8x8 pour faciliter son insertion visuelle et le retourne
    constructor(board: BoardRepresentation) {
        this.cases = new Array<Array<PieceRepresentation>>();
        var cloneBoardCases = board.cases.slice();
        while (cloneBoardCases.length > 0) {
        const arrayElement = cloneBoardCases.splice(0, 8);
        this.cases.push(arrayElement);
      }
    }
}
