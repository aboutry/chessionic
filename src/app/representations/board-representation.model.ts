import { PieceRepresentation } from './piece-representation.model';

/**
 * BoardRepresentation
 *
 * @export
 * @class BoardRepresentation
 */
export class BoardRepresentation {
    public cases: Array<PieceRepresentation>;

    constructor() {
        this.cases = [];
        const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
        // Créé chaque case du plateau => a1 a2 a3 .... b1 b2 b3 ......
        for (let i = 8; i > 0; i--)
            {
                alphabet.forEach(element => {
                this.cases.push(new PieceRepresentation(null, element + (i).toString(), null));
            });
        }
    }
}
