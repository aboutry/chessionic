/**
 * PieceRepresentation
 *
 * @export
 * @class PieceRepresentation
 */
export class PieceRepresentation {
    public id: string;
    public type: string;
    public position: string;
    public color: string;
    public background: string;
    public isAlife: boolean = true;
    public initiale: boolean;

    constructor(type: string, position: string, color: string){
        this.id = position;
        this.type = type;
        this.position = position;
        this.color = color;
        if(!this.type)
        {
            this.isAlife = false;
        }
        this.background = position ? this.backgroundColor(position) : null;
        this.initiale = true;
    }

    // Render backgroudColor pour les cases
    public backgroundColor(caseId: string): string
    {
      if(['a','c','e','g'].indexOf(caseId[0]) !== -1)
      {
        return Number(caseId[1])%2 == 0 ?  'white' : 'lightgrey';
      }
      return Number(caseId[1])%2 == 0 ?  'lightgrey' : 'white';
    }
}