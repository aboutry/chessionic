import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PieceRepresentation } from '../representations/piece-representation.model';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss'],
})
export class PromoComponent implements OnInit {
  public listPromo: Array<PieceRepresentation>;

  @Input() set color(pieceColor: string) {
    this.generatePromo(pieceColor);
  }

  @Output() promoEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  // Prépare les affichages des promos dispos
  private generatePromo(color: string)
  {
    this.listPromo = [];
    this.listPromo[0] = new PieceRepresentation('tower', null, color);
    this.listPromo[1] = new PieceRepresentation('knight', null, color);
    this.listPromo[2] = new PieceRepresentation('bishop', null, color);
    this.listPromo[3] = new PieceRepresentation('queen', null, color);
  }

  // Event Emitter pour la selection d'une promotion
  public selectPromo($event, type: string)
  {
    this.promoEvent.emit(type);
  }
}
