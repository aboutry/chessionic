import { Component, OnInit } from '@angular/core';
import { BoardRepresentation } from '../representations/board-representation.model';
import { PieceRepresentation } from '../representations/piece-representation.model';
import { PartieService } from '../services/partie/partie.service';
import { Subscription } from 'rxjs';
import { BoardChunkRepresentation } from '../representations/board-chunk-representation.model';
import { MoveService } from '../services/move/move.service';
import { Plugins } from '@capacitor/core';

const { App } = Plugins;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {

    // Variables
    public chessBoard: BoardChunkRepresentation;
    public chess: BoardRepresentation;
    public selectedPiece: PieceRepresentation;
    public currentPlayer: string;
    public playableCases: Array<string>;
    public waitingForPromo: PieceRepresentation;
    public message: string;

    // Subscriptions
    private currentPlayerSubscription: Subscription;
    private chessBoardSubscription: Subscription;

    constructor(
      private partieService: PartieService,
      private moveService: MoveService) {
        this.waitingForPromo = null;
      }
    
    ngOnInit() {
      this.restart();

      // Observable sur le joueur actuel
      this.currentPlayerSubscription = this.partieService.getCurrentPlayer().subscribe((player) => {
        this.currentPlayer = player;
      })

      // Observable sur le plateau à afficher
      this.chessBoardSubscription = this.partieService.getChessBoardChrunked().subscribe((board) => {
        this.chessBoard = board;
      })
    }

    // unsubscribe des observables
    ngOnDestroy(): void {
      this.currentPlayerSubscription.unsubscribe();
      this.chessBoardSubscription.unsubscribe();
    }

    // Fonction onClick
    public onClickChess(event, item: PieceRepresentation)
    {
      if(item.background === "orange")
      {
        // Deplacement roque
        if(this.selectedPiece.type === "king" && this.selectedPiece.initiale)
        {
          switch (item.position) {
            case "g1":
              this.deplacer(this.chess.cases[63], this.chess.cases[61]);
              break;
            case "c1":
              this.deplacer(this.chess.cases[56], this.chess.cases[59]);
              break;
            case "c8":
              this.deplacer(this.chess.cases[0], this.chess.cases[3]);
              break;
            case "g8":
              this.deplacer(this.chess.cases[7], this.chess.cases[5]);
              break;
          }
        }
        this.deplacer(this.selectedPiece, item);
        this.resetCasesBackgroudColor();
        if(this.moveService.getPromo(item))
        {
          this.waitingForPromo = item;
        }
        else
        {
          this.partieService.setCurrentPlayer(this.currentPlayer === 'white' ? 'black' : 'white');
        }
        this.setMessage();
        if(this.waitingForPromo)
        {
          this.partieService.setCurrentPlayer(null);
        }
      }
      else if(this.currentPlayer === item.color)
      {
        if(item.type)
        {
          this.resetCasesBackgroudColor();
          this.selectedPiece = item;
          this.changeBackgroundColor(this.selectedPiece, 'green');
          this.playableCases = this.moveService.getAvailableMoves(item, this.chess.cases);
          this.displayPlayableCases(this.playableCases);
        }
      }
    }

    // fonction déplacement d'un pion
    public deplacer(fromPiece: PieceRepresentation, toPiece: PieceRepresentation)
    {
      let itemClone = new PieceRepresentation(fromPiece.type, fromPiece.position, fromPiece.color);
      itemClone.id = fromPiece.id;

      fromPiece.type = null;
      fromPiece.color = null;
      fromPiece.isAlife = false;
      fromPiece.id = itemClone.id;

      toPiece.type = itemClone.type;
      toPiece.color = itemClone.color;
      toPiece.isAlife = itemClone.isAlife;
      toPiece.id = itemClone.id;

      fromPiece.initiale = false;
      toPiece.initiale = false;
    }

    // Reset cases backgroud color
    public resetCasesBackgroudColor()
    {
      this.chess.cases.forEach(ChessCase => {
        this.changeBackgroundColor(ChessCase);
      });
    }

    // Render for playable cases
    public displayPlayableCases(cases: Array<string>)
    {
      this.chess.cases.forEach(ChessCase => {
        cases.forEach(playableCase => {
          if(playableCase === ChessCase.position)
          {
            this.changeBackgroundColor(ChessCase, "orange");
          }
        });
      });
    }

    // Render backgroudColor pour les cases
    public changeBackgroundColor(piece: PieceRepresentation, couleur: string = null)
    {
      if(couleur) {
        piece.background = couleur;
      }
      else piece.background = piece.backgroundColor(piece.position);
    }

    // Indique la fin de partie ou la mise en échec
    public setMessage(){
      if (this.moveService.getGameOver(this.chess.cases, this.currentPlayer)){
        let color: string = this.currentPlayer === "white" ? "noir" : "blanc";
        this.partieService.setCurrentPlayer(null);
        if (this.moveService.getMate(this.chess.cases)){
          this.message = "Echec et mat, joueur " + color + " gagne !"
        } else {
          this.message = "Partie nulle : pat !";
        }
      } else {
        let color: string = this.currentPlayer === "white" ? "blanc" : "noir";
        this.message =  (this.moveService.getMate(this.chess.cases)) ?
        "Attention, joueur " + color + " en échec !" :
        null;
      }
    }

    // Reprend la partie après promotion d'un pion
    public onChangePromo(promo: string){
      this.waitingForPromo.type = promo;
      this.partieService.setCurrentPlayer(this.waitingForPromo.color === 'white' ? 'black' : 'white');
      this.setMessage();
      this.waitingForPromo = null;
    }

    // Positionne les pions ou reset les positions
    public restart()
    {
      let pieces = new Array<PieceRepresentation>();
      this.chess = new BoardRepresentation();

      // Génére une partie, place les pièces sur un nouveau plateau et les set dans le service
      this.partieService.getDefaultBoardJson().subscribe(data =>{
        data.forEach(element => {
          pieces.push(new PieceRepresentation(element.type, element.position, element.color));
        });
        this.chess.cases = this.partieService.setPiecesOnBoard(this.chess, pieces);
        this.partieService.setChessBoard(this.chess);
      });

      this.partieService.setCurrentPlayer('white');
      this.waitingForPromo = null;
      this.message = null;
    }

    // Quitte l'app
    public exitApp()
    {
      App.exitApp();
    }
}